package com.hcl.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.model.Customer;
import com.hcl.services.RegisterServiceImpl;

@RestController
@RequestMapping("/register")
public class RegisterUser {
	@Autowired
	private RegisterServiceImpl service;

	@PostMapping("/registerCustomer")
	@CrossOrigin(origins = "http://localhost:4200")
	public Customer registerUser(@RequestBody Customer customer) throws Exception {
		String email = customer.getEmail();

		Customer customer1 = service.getCustomer(email);

		if (customer1 != null) {

			throw new Exception("Email id already exist");
		}

		Customer customer21 = service.registerUser(customer);

		return customer21;

	}

	@PostMapping("/login")
	@CrossOrigin(origins = "http://localhost:4200")
	public Customer loginUser(@RequestBody Customer customer) throws Exception {

		String email = customer.getEmail();
		String password = customer.getPassword();
		Customer customer1 = service.fetchUserByEmailAndPassword(email, password);
		if (email != null && password != null) {
			customer1 = service.fetchUserByEmailAndPassword(email, password);
		}
		if (customer1 == null) {
			throw new Exception("bad credentials");
		}
		return customer1;
	}

}
