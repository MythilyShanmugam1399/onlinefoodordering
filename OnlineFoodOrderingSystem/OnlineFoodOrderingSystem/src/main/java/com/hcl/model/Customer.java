package com.hcl.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "customer")

public class Customer {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@GenericGenerator(name = "native", strategy = "native")

	@Column(name = "customer_id")
	private int customer_id;
	@Column(name = "first_name")
	private String firstName;
	@Column(name = "last_name")
	private String lastName;
	@Column(name = "email")
	private String email;
	@Column(name = "password")
	private String password;
//	
//	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
//	@JoinColumn(name="address_id")
//	private Address address;
//	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
//	@JoinColumn(name="payment_id")
//    private Payment payment;
//	@OneToOne
//	@JoinColumn(name="order_id")
//    private OrderDetails orderDetails;
//	
//	@OneToMany
//	
//	@JoinColumn(name="order_id")
//    private Set<OrderDetails> orderdetails=new HashSet<>();
	
	
	
//	@OneToMany(mappedBy = "customer", cascade = CascadeType.ALL)
//    private Set<OrderDetails> orders = new HashSet<>();
//	public void add(OrderDetails order) {
//        if (orders == null) {
//            orders = new HashSet<>();
//        } else if (order != null) {
//            orders.add(order);
//            order.setCustomer(this);
//        }
//    }
	
	

}
