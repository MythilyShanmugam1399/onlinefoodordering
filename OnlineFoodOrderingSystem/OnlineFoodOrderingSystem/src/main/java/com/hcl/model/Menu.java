package com.hcl.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "menu")
public class Menu {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)

	@Column(name = "menu_id")
	private int menuId;
	@Column(name = "menu_name")
	private int menuName;
	@Column(name = "unit_price")
	private double unitPrice;
	@Column(name = "category_id")
	private int categoryId;

}
