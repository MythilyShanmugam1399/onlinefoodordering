package com.hcl.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "orderDetails")
public class OrderDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)

	@Column(name = "order_id")
	private int orderId;
	@Column(name = "total_price")
	private double totalPrice;
	@Column(name = "total_quantity")
	private int totalQuantity;
//	@OneToOne
//	@JoinColumn(name="address_id")
//	private Address address;
//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name="customer_id")
//	private Customer customer;
	
//	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
//    @JoinColumn(name = "customer_id")
//    private Customer customer;
//	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
//   @JoinColumn(name = "customer_id")
//   private Customer customer;
//	
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id")
    private Address address;
    
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name="card_id")
    private CreditCard creditCard;

}
