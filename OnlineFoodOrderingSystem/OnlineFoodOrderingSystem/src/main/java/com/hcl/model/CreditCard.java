package com.hcl.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "creditCard")
public class CreditCard {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "card_id")
	private int cardId;
	@Column(name = "card_Type")
	private String cardType;
	@Column(name = "NameOfCard")
	private String nameOfCard;
	@Column(name = "card_Number")
	private long cardNumber;
	@Column(name = "security_Code")
	private int securityCode;
	@Column(name = "expiration_Month")
	private int expirationMonth;
	@Column(name = "expiration_Year")
	private int expirationYear;
}
