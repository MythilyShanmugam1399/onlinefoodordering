package com.hcl.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "cart")

public class Cart {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "cart_id")
	private int cartId;
	@Column(name = "order_id")
	private int orderId;
	@Column(name = "quantity")
	private String quantity;
	@Column(name = "menu_id")
	private int menuId;
	@Column(name = "unit_price")
	private double unitPrice;

	@OneToMany
	List<OrderDetails> orderDetails;

}
