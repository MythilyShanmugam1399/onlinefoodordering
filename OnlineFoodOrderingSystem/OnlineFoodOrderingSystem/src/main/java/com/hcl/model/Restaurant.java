package com.hcl.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "restaurant")
public class Restaurant {
	

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	

	@Column(name = "restaurant_id")
	private int restaurantId;
    @Column(name = "restaurant_name")
	private String restaurantName;
    
    
    @OneToMany
    List<Menu> menu;

}

