package com.hcl.dto;

import com.hcl.model.Address;
import com.hcl.model.CreditCard;
import com.hcl.model.Customer;
import com.hcl.model.OrderDetails;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Purchase {
	
	private Customer customer;
	private Address address;
	private CreditCard creditCard;
	private OrderDetails orderDetails;

}
