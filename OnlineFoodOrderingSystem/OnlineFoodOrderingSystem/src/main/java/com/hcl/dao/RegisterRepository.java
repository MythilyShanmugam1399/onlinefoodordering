package com.hcl.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.model.Customer;

@Repository
public interface RegisterRepository extends JpaRepository<Customer, Integer> {
	
	Customer findByEmail(String Email);
	
	Customer save(Customer customer);
	Customer findByEmailAndPassword(String email, String password);

}
