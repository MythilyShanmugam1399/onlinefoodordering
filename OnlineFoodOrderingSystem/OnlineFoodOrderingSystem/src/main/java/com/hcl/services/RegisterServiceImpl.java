package com.hcl.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.dao.RegisterRepository;
import com.hcl.model.Customer;

@Service
public class RegisterServiceImpl implements IRegisterService {

	@Autowired
	public RegisterRepository repository;

	public Customer getCustomer(String email) {
		Customer customer = repository.findByEmail(email);
		return customer;
	}

	public Customer registerUser(Customer customer) {
		Customer customer1 = repository.save(customer);
		return customer1;
	}

		
	

	public Customer fetchUserByEmailAndPassword(String email, String password) {
		// TODO Auto-generated method stub
		return repository.findByEmailAndPassword(email, password);
	}

}
