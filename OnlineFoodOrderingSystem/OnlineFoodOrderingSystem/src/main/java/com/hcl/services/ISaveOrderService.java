package com.hcl.services;

import com.hcl.dto.Purchase;

public interface ISaveOrderService {
	
	void saveOrderDetails(Purchase purchase);
	

}
