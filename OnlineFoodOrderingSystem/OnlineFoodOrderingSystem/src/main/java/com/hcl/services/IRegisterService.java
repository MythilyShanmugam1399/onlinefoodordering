package com.hcl.services;

import com.hcl.model.Customer;

public interface IRegisterService {

Customer registerUser(Customer customer);
Customer getCustomer(String email);
}


