import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Restaurant } from '../model/restaurant';
import { CartService } from '../service/cart.service';
import { SearchService } from '../service/search.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  dish:string="";
 
  total_id:any="0";
 // cart:CartService;
  restaurant!:Restaurant;
  totalPrice:any=0;
  totalQuantity:any="0";
  btn_cart:boolean=false;
  loginSuccess!:string;
  enableProp!:number;

  route: ActivatedRoute | null | undefined;
  cart: CartService;
  


  

constructor(public router:Router,public service:SearchService,public cartService:CartService){
  this.cart=this.cartService;
  localStorage.clear();
 


 
}
  ngDoCheck(){ 

   this.totalPrice=localStorage.getItem('totalPrice');
   this.totalQuantity=localStorage.getItem('totalQuantity');
   if(this.totalPrice > 0)
   {
     this.btn_cart=true;
   }
   //Here we Have put the login and signup button enable and disable
   this.loginSuccess=this.cartService.getLoginSuccess();
   this.enableProp=this.cartService.getSuccessFlag();
  }
  

onLogout(){
//this.enableProp=1;
location.reload();
}

  ngOnInit():void { }

  //For getting the search button Method so that we can Search the dish
  onEnter(value:any)
   
  
  { 
    this.service.storeData(value);
    this.router.navigate(['search'],{relativeTo:this.route});
  }

}
