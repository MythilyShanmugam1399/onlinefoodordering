import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Customer } from '../model/customer';
import { User } from '../model/user';
import { RegistrationService } from '../service/registration.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

customer= new Customer();
msg='';

  constructor(private _service : RegistrationService, private _router : Router) { }

  ngOnInit(): void {
  }
 
  registerCustomer()
  {
  this._service .registerUser(this.customer).subscribe(
  _data =>{
    alert("Register Successfully!");
    this._router.navigate(['']);
    
  },
  error => {
   
  this.msg=error.error;
  alert("customer already registered");
    
  }
  
)
  }
}