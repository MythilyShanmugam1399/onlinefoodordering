import { Injectable } from '@angular/core';
import { error } from '@angular/compiler/src/util';
import { Observable } from 'rxjs';
import { Customer } from '../model/customer';
import {HttpClient} from '@angular/common/http'
 

@ Injectable({
  providedIn: 'root'
})
export class RegistrationService
 {
  

  constructor( private _http : HttpClient) {
   }


   
   public registerUser(customer :Customer):Observable<any>
   {
     console.log(customer);
     return this._http.post<any>("http://localhost:8088/register/registerCustomer",customer);
   }
   public loginUser(customer :Customer):Observable<any>{
    return this._http.post<any>("http://localhost:8088/register/login",customer)
  }
   handleError(error : Response)
   { }
}
