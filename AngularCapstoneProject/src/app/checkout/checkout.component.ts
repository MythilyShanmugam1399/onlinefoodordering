import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Address } from '../model/address';
import { CreditCard } from '../model/creditcard';
import { Customer } from '../model/customer';
import { OrderDetails } from '../model/orderdetails';
import { Purchase } from '../model/purchase';
import { User } from '../model/user';
import { CheckoutService } from '../service/checkout.service';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {

  user = new User();
  address= new Address();
  orderDetails=new OrderDetails();
  purchase!: Purchase;
  customer = new Customer();
  creditCard = new CreditCard();
 
 
  constructor(public checkoutService:CheckoutService,public route:Router) { }
  totalPrice:any=0;
  totalQuantity:any="0";
  
  ngOnInit(): void {
   
  }
  
  ngDoCheck(){
    this.totalPrice=localStorage.getItem('totalPrice');
    this.totalQuantity=localStorage.getItem('totalQuantity');
    
   }

 register(_data:any){

  this.orderDetails.totalPrice=this.totalPrice;
  this.orderDetails.totalQuantity=this.totalQuantity;
  
  this.purchase = new Purchase(this.customer,this.address,this.creditCard,this.orderDetails);

  this.checkoutService.checkout(this.purchase).subscribe(
  _data =>{
    alert("YOUR ORDER IS GOT PLACED");
    localStorage.clear();
    this.route.navigate(['']);
  },
  _error => {
    alert("SOMETHING WENT WRONG");
  })
}
}

