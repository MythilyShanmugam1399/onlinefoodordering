import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CartitemsComponent } from './cartitems/cartitems.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { Restaurant1Component } from './restaurant1/restaurant1.component';
import { SearchComponent } from './search/search.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { SliderComponent } from './slider/slider.component';


const routes: Routes = [
  {path: '',component: SidebarComponent, children : [
    {path: 'rest-1/:AdiGas',component: Restaurant1Component},
    {path: 'rest-2/:RotiGhar',component:Restaurant1Component},
    {path: 'rest-3/:PanjabiRasoi',component:Restaurant1Component},
    {path: 'rest-4/:Udupi',component:Restaurant1Component},
    {path:'login',component:LoginComponent},
    {path:'registration',component:RegistrationComponent},
   
    {path:'cartitems',component:CartitemsComponent},
    {path:'check',component:CheckoutComponent},
    {path:'search',component:SearchComponent},
    {path:'',component:SliderComponent}
  ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
