import { OrderDetails } from "./orderdetails";

export class Customer {
    customer_id:number=0;
    firstName:string="";
    lastName:string="";
    email:string="";
    password:string="";
    orderDetails:OrderDetails=new OrderDetails();
    
    constructor(){}
}
