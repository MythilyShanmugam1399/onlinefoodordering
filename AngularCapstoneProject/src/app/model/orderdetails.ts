import { Address } from "./address";
import { CreditCard } from "./creditcard";



export class OrderDetails {
    orderId:number=0;
    totalPrice: number=0;
    totalQuantity:number=0;
  
    address:Address=new Address();
    creditCard:CreditCard=new CreditCard();
    constructor(){}
}