import { Address } from "./address";
import { CreditCard } from "./creditcard";
import { Customer } from "./customer";
import {  OrderDetails } from "./orderdetails";
;

export class Purchase {
    customer:Customer=new Customer;
    orderDetails:OrderDetails=new OrderDetails;
    address:Address=new Address;
    creditCard:CreditCard=new CreditCard;
    constructor(customer:Customer,address:Address,creditCard:CreditCard,
        orderDetails:OrderDetails
        ){
            this.orderDetails=orderDetails;
            this.customer=customer;
            this.address=address;
            this.creditCard=creditCard;

    }

}