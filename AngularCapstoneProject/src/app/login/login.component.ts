import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Customer } from '../model/customer';


import { User } from '../model/user';
import { CartService } from '../service/cart.service';
import { RegistrationService } from '../service/registration.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  customer = new Customer();
  msg='';
  message:string="welcome"+this.customer.firstName;
  flag:number=0;
  constructor( private _service : RegistrationService,private _route:Router,private cartService:CartService) { }

  ngOnInit(): void {
  }

  //This method is used for seding the login data to the service for process
  loginUser(){
    this._service.loginUser(this.customer).subscribe(
      data => 
      {
        this.flag=1;
        this.cartService.onLoginSuccess(data.firstName+" "+data.lastName , this.flag)
        this._route.navigate([''])
      },
      error =>{
         console.log("exceptions occured");
        this. msg="bad credentials";
      }
    )
   
  }
  }
  

