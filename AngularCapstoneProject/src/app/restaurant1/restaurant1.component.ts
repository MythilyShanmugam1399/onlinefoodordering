import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Restaurant } from '../model/restaurant';
import { CartService } from '../service/cart.service';
import restaurants from '../json/restaurant.json';

@Component({
  selector: 'app-restaurant1',
  templateUrl: './restaurant1.component.html',
  styleUrls: ['./restaurant1.component.css']
})
export class Restaurant1Component implements OnInit {

  restaurant:Restaurant[];
 rname:any="";
 dishid: any="";

  constructor(private route: ActivatedRoute, public router:Router,public cartService:CartService) {
   this.restaurant=restaurants;   
   }

  ngOnInit(): void {
    if(this.route.snapshot.paramMap.get('AdiGas') === "AdiGas")
    this.rname = "AdiGas";
    else if(this.route.snapshot.paramMap.get('RotiGhar') === "RotiGhar")
    this.rname = "RotiGhar";
    else if(this.route.snapshot.paramMap.get('PanjabiRasoi') ==="PanjabiRasoi")
    this.rname = "PanjabiRasoi";
    else if(this.route.snapshot.paramMap.get('Udupi') ==="Udupi")
    this.rname="Udupi";
  }

ngOnChange()
{
  localStorage.clear();
}
//Method for Adding the value to the cart
  OnAddCart(dishId:string){
   this.cartService.storeCart(dishId);
  }

}
