import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

AdiGas : string = "AdiGas";
RotiGhar : string = "RotiGhar";
PanjabiRasoi : string = "PanjabiRasoi";
Udupi : string = "Udupi";

//This Constructor is used for routing to restaurant and divide it on the basis of id 
  constructor( public router :Router) {
    this.router.navigate(['/rest-1',this.AdiGas]);
    this.router.navigate(['/rest-2',this.RotiGhar]);
    this.router.navigate(['/rest-3',this.PanjabiRasoi]);
    this.router.navigate(['/rest-4',this.Udupi]);
    this.router.navigateByUrl('');
   }


  ngOnInit(): void {
  }
}
